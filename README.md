# Flask Practice

### To run the app for the first time:
1. `python3 -m venv venv`
2. `source venv/bin/activate`
3. `pip install -r requirements.txt`
4. `flask db init`
5. `flask db migrate`
6. `flask db upgrade`
7. `flask run`
8. view in http://localhost:5000/

